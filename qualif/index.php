<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="iso-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">

		<title>Simulateur d'avis produits</title>

		<link rel="icon" type="image/x-icon" href="favicon.ico">

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.2/css/tether.min.css" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-mac-osx.min.css" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.6/css/fileinput.min.css" />
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<link rel="stylesheet" href="css/select2/select2-bootstrap.min.css" />
		<link rel="stylesheet" href="css/vertical-tabs/bootstrap.vertical-tabs.min.css" />
		<link rel="stylesheet" href="css/style.css"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.formvalidation/0.6.1/css/formValidation.min.css">

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.2/js/tether.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.formvalidation/0.6.1/js/formValidation.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.formvalidation/0.6.1/js/framework/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/functions.js"></script>

	</head>
	<body>
		<div class="container">
		<h1>Simulateur d'avis produits</h1>

		<div class="col-md-3">
		<!-- Nav tabs -->
			<ul class="nav nav-tabs tabs-left">
				<li class="active">
					<a href="#display" data-toggle="tab">Visualiser</a>
				</li>
				<li class="disabled" >
					<!-- <a href="#generate" data-toggle="tab" disabled>G&eacute;n&eacute;rer</a> -->
					<a href="#">G&eacute;n&eacute;rer</a>
				</li>
			</ul>
		</div>

		<div class="col-md-9">
		<!-- Tab panes -->
			<form id="avisProduitsForm" class="form-horizontal">
				<div class="form-group row">
					<select id="SiteID" name="SiteID" ></select>
				</div>
				<div class="form-group row">
					<select id="ProduitClientID" name="ProduitClientID" ></select>
				</div>
			</form>
			<div class="tab-content">
				<div class="tab-pane active" id="display">
					<div class="form-group row">
						<input type="number" class="form-control" id="NbPerPage" name="NbPerPage" value="1" min="1">
					</div>
					<div class="form-group row">
						<button type="button" class="btn btn-outline-success btn-block bold" id="loadAvis">Soumettre</button>
						<button type="reset" class="btn btn-outline-warning btn-block bold clearForm">Effacer</button>
					</div>
				</div>
				<div class="tab-pane" id="generate">
					<div class="form-group row">
						<button type="button" class="btn btn-outline-primary btn-block bold" id="generateAvis" >G&eacute;n&eacute;rer</button>
						<button type="reset" class="btn btn-outline-warning btn-block bold clearForm">Effacer</button>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
