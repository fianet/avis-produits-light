function getBaseUrl() {
	var re = new RegExp(/^.*\//);
	return re.exec(window.location.href)[0];
}

function getURI(uri) {
	return getBaseUrl() + uri;
}

function getPartialView(template) {

	// Return a new promise.
	return new Promise(function (resolve, reject) {
		let req = new XMLHttpRequest();
		req.open('GET', getURI('/templates/' + template + '.html'), true);

		req.onreadystatechange = function () {

			if (req.readyState != 4 || req.status != 200) {
				return;
			}
			resolve(req.responseText);

		};

		// Handle network errors
		req.onerror = function () {
			reject(Error("Network Error"));
		};

		// Make the request
		req.send();
	});
}

let getJSON = function (url, callback) {
	let xhr = new XMLHttpRequest();
	xhr.open('GET', url, true);
	xhr.responseType = "json";
	xhr.onload = function () {
		let status = xhr.status;
		if (status == 200) {
			callback(null, xhr.response);
		} else {
			callback(status);
		}
	};
	xhr.send();
};

function getPageAvis(askedPage) {

	let avisProduitsScript = document.getElementById('avis-produits-script');
	let siteId = avisProduitsScript.getAttribute('data-site');
	let productId = avisProduitsScript.getAttribute('data-produit');
	let nbPerPage = avisProduitsScript.getAttribute('data-nb');

	let hashedProductId = md5(productId);

	if (siteId && productId) {
		getJSON(getURI('/json/' + siteId + '/' + hashedProductId + '.json'),
			function (err, data) {
				if (err != null) {
					// alert("Something went wrong: " + err);
					return;
				}
				let pager = new Object;

				//Parse datetimes to the french format
				if (data.Avis) {
					if (nbPerPage && askedPage && data.NbrAvis) {

						// nbPerPage in range[1, data.NbrAvis] ?
						if (nbPerPage < 1) {
							nbPerPage = 1;
						} else if (nbPerPage > data.NbrAvis) {
							nbPerPage = data.NbrAvis;
						}
						let nbPages = Math.ceil(data.NbrAvis / nbPerPage);

						// askedPage in range[1, nbPages] ?
						if (askedPage > nbPages) {
							askedPage = nbPages;
						} else if (
									(askedPage < 1)
								||
									(
											(nbPerPage == data.NbrAvis)
										&&
											(askedPage > 1)
									)
						) {
							askedPage = 1;
						}

						let firstIndex = (nbPerPage * (askedPage - 1));
						let lastIndex = (nbPerPage * askedPage);

						// console.log('nbPerPage = ', nbPerPage);
						// console.log('askedPage = ', askedPage);
						// console.log('nbPages = ', nbPages);
						// console.log('firstIndex = ', firstIndex);
						// console.log('lastIndex = ', lastIndex);
						// console.log(data.Avis);

						data.Avis = data.Avis.slice(firstIndex, lastIndex);

						pager.current = askedPage;
						pager.limit = nbPages;

						pager.previous = pager.current - 1;
						pager.previousClass = '';
						if (pager.previous < 1) {
							pager.previous = 1;
							pager.previousClass = 'invisible';
						}

						pager.next = pager.current;
						if (pager.next < pager.limit) {
							 pager.next++;
						}
						pager.nextClass = '';
						if (pager.current == pager.limit) {
							pager.nextClass = 'invisible';
						}

						data.pager = pager;
					}
				}

				if (data.Moyenne && data.NbrAvis && data.Avis && data.Avis.constructor === Array) {
					getPartialView('templateAvis').then(function (result) {
						let avisTemplate = doT.template(result);
						document.getElementById('fianet-avis-section').innerHTML = avisTemplate(data);
					});
				}
			}
		);
	}
}

window.onload = function () {
	getPageAvis(1);
}
