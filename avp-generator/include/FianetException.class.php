<?

class FianetException extends Exception {
	// public $trace;

	function __construct($statusCode = 0, $statusMessage = 0) {
		$this->setStatusCode($statusCode);
		$this->setStatusMessage($statusMessage);
	}

	public function setStatusCode($statusCode) {
		if (empty($statusCode)) {
			$statusCode = 500;
		}
		$this->code = $statusCode;
	}

	public function setStatusMessage($statusMessage) {
		$this->message = $statusMessage;
		return ($this);
	}

	public function setHttpHeader() {
		$this->unsetXdebugMessage();
		header('HTTP/1.0 ' . $this->code . ' ' . $this->message);
	}

	public function unsetXdebugMessage() {
		unset($this->xdebug_message);
	}
}
