<?php
	define ('CIPHER_WS', true);
	define ('DEBUG_MODE', false);

	require ('include/ws.inc.php');

	define ('STATE_AUTH_OK', 0);
	define ('STATE_ERR_INTERNAL', -50);
	define ('STATE_ERR_PARAM', -10);
	define ('STATE_AUTH_FORBIDDEN', 10);

	$db = new Database;

	try {
		header ('Content-Type: text/xml; charset="UTF-8"');
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";

		$accessGranted = 0;
		$statusCode = STATE_ERR_INTERNAL;
		$statusMsg = "Erreur interne.";

		$login = stripslashes ($_REQUEST['login']);
		$passwd = stripslashes ($_REQUEST['passwd']);
		$app_id = stripslashes ($_REQUEST['app_id']);
		$ip_addr = stripslashes ($_REQUEST['ip']);

		if ($app_id == '' || !ctype_digit($app_id)) {
			throw new ParamError ('app_id');
		}
		if ($login == '') {
			throw new ParamError ('login');
		}
		if ($passwd == '') {
			throw new ParamError ('passwd');
		}
		if ($ip_addr == '') {
			throw new ParamError ('ip');
		}

		$app_id = (int) $app_id;

		$db->connect (WSDB_DSQUERY, WSDB_USER, WSDB_PASSWD, WSDB_DATABASE, "UTF-8", false);

		$res = $db->exec (
			"exec giveAuthorisation "
			. "'".	sqltext ($login)	."', "
			. "'".	sqltext ($passwd)	."', "
			. "'".	sqltext ($ip_addr)	."', "
			. (int) $app_id
			.", 1" // 1 pour indiquer que le mdp est d�j� chiffr�.
		);
		list ($isOK, $userID, $reason) = $res->rowfetch();

		if (! $isOK) {
			$statusMsg = $reason;
			$statusCode = STATE_AUTH_FORBIDDEN;

		} else {
			// Charger les profils du compte...
			$userID = (int) $userID;

			if ($app_id == 10 || $app_id == 11) {
				$profiles = $db->select (
					" select KwixoProfileID as id".
					" from UserMapKwixo U, UserMapKwixoProfile P ".
					" where U.UserID = {$userID} and U.AppliID = {$app_id} ".
					" and P.UserID = U.UserID and P.AppliID = U.AppliID"
				);
			}

			$accessGranted = 1;
			$statusCode = STATE_AUTH_OK;
			$statusMsg = $reason;
		}

	} catch (DatabaseException $e) {
		$statusCode = STATE_ERR_INTERNAL;
		$statusMsg = $e->getMessage();

		if (DEBUG_MODE === true && $e->hasQuery()) {
			$statusMsg .= ' "'.$e->getQuery(). '"';
		}

	} catch (Exception $e) {
		$statusCode = $e->getCode();
		$statusMsg = $e->getMessage();
	}

?>
<authorization granted="<?= (int)$accessGranted ?>">
	<reason code="<?= $statusCode ?>"><?= xmltext ($statusMsg); ?></reason><?
	if (is_array($profiles) && count($profiles) > 0) {

		echo "<profiles nb=\"".count($profiles)."\">\n";
		foreach ($profiles as $prof) {
			echo '<profile>'. xmltext($prof['id']).'</profile>';
		}

		echo "</profiles>\n";
	}
	if (defined ('DEBUG_MODE') && DEBUG_MODE == true) {
		if ($db->hasMessages()) {
			echo '<db_log>';
			foreach ($db->getMessages() as $msg) {
				echo '<log date="'.$msg['date'].'">'.xmltext($msg['text']).'</log>';
			}
			echo '</db_log>';
		}
	}
?>
</authorization>
