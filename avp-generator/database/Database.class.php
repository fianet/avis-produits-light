<?php

class DatabaseException extends Exception
{
	private $sql = NULL;

	function __construct($message, $sql = NULL, $code = 0) {
		$this->sql = $sql;
		parent::__construct("Database: ".$message, $code);
	}

	public function hasQuery() {
		return ($this->sql != NULL);
	}
	public function getQuery() {
		return $this->sql;
	}
}

class DbResult
{
	private $server = NULL;
	private $resource = NULL;
	private $my_rows = NULL;

	public function __construct (&$server, &$resource)
	{
		$this->server = $server;
		$this->resource = $resource;
	}

	public function rows() {
		return $this->my_rows;
	}

	public function fetchAll()
	{
		if ($this->my_rows !== NULL) {
			unset ($this->my_rows);
		}
		$this->my_rows = array();

		while ( ($row = @ sybase_fetch_array ($this->resource)) !== FALSE) {
			$this->my_rows[] = $row;
		}

		return $this->my_rows;
	}
	public function fetchAllAssoc()
	{
		if ($this->my_rows !== NULL) {
			unset ($this->my_rows);
		}
		$this->my_rows = array();

		while ( ($row = @ sybase_fetch_assoc ($this->resource) ) != NULL) {
			$this->my_rows[] = $row;
		}

		return $this->my_rows;
	}

	public function arrayfetch() {
		return @ sybase_fetch_array ($this->resource);
	}
	public function assocfetch() {
		return @ sybase_fetch_assoc ($this->resource);
	}
	public function rowfetch() {
		return @ sybase_fetch_array ($this->resource);
	}
	public function objectfetch() {
		return @ sybase_fetch_object ($this->resource);
	}
	public function rowseek ($offset) {
		return @ sybase_data_seek ($this->resource, $offset);
	}
	public function nbAffectedRows() {
		return @ sybase_affected_rows($this->resource);
	}

	public function count() {
		return @ sybase_num_rows($this->resource);
	}
};


class Database
{
	private $handle = NULL;
	private $resultCache = NULL;
	private $cached = NULL;
	private $dsquery = NULL;
	private $serverMsg = NULL;
	private $tranCount = 0;

	function messageHandler ($number, $severity, $state, $line, $text)
	{
		$this->serverMsg[] = array (
			'date' => date ('d/m/Y H:i:s'),
			'info' => '['. $number .' '. $severity .' '. $state .' '. $line .']',
			'text' => $text
		);
		return TRUE;
	}

	function __destruct()
	{
		$this->close();
	}

	function __construct()
	{
		$this->serverMsg = array();
	}

	public function getHandle() {
		return $this->handle;
	}

	public function hasMessages() {
		return (isset($this->serverMsg) && count($this->serverMsg) > 0);
	}
	public function getMessages() {
		return $this->serverMsg;
	}

	/**
	 * Ouvre une connexion � un serveur, avec un login et mot de passe donn�s.
	 * La fonction accepte �ventuellemen un nom de base de donn�es � utiliser.
	 * @param $server le nom du serveur auquel on essaye de se connecter.
	 * @param $user le nom de l'utilisateur qui se connecte.
	 * @param $pass le mot de passe de l'utilisateur.
	 * @param $dbame contient le nom de la base de donn�es � utiliser apr�s la connexion, NULL pour utiliser la base attribu�e par d�faut par le serveur.
	 * @param $charset contient �ventuellement le nom du jeu de caract�res � utiliser pour la connexion.
	 * @param $useCache indique si le cache de requ�te et de r�sultat est utilis� (TRUE) ou non.
	 * @param $appName indique le nom d'application � fournir au serveur, pour identifier la connexion.
	 * @throw Exception si la connexion n'a pas pu se faire.
	 */
	function connect ($server, $user, $pass, $dbname = NULL, $charset = NULL, $useCache = FALSE, $appName = NULL)
	{
		if ($this->handle == NULL) {

			$this->dsquery = $server;

			if ( FALSE === ($this->handle = @ sybase_connect ($server, $user, $pass, $charset, $appname)) ) {
					throw new DatabaseException ('['. $this->dsquery. ']: sybase_connect() failed.');
			}

			if ( FALSE === @ sybase_set_message_handler (array($this, "messageHandler"), $this->handle) ) {
					// $LOG::warn ("sybase_set_message_handler() failed.");
			}

			if($dbname) {
				$this->use_db ($dbname);
			}

			$this->cached = $useCache;
			$this->resultCache = array();
			$this->tranCount = 0;
		}
	}

	/**
	 * Ferme la connexion courante. Aucun effet si la connexion �tait d�j� ferm�e.
	 */
	function close()
	{
		if ($this->handle !== NULL) {
			if ($this->tranCount > 0) {
				$this->rollbackTran();
			}

			@ sybase_close ($this->handle);
			$this->handle = NULL;
		}
	}

	/**
	 * Change la base de donn�e � utiliser sur le serveur.
	 * @param dbname le nom de la base � utiliser.
	 * @throw Exception lorsque la commande a �chou�.
	 */
	function use_db ($dbname)
	{
		if ( sybase_select_db ($dbname, $this->handle) === FALSE ) {
			throw new DatabaseException ('['. $this->dsquery. ']: could not use '. $dbname);
		}
	}

	/**
	 * Effectue une requ�te de type SELECT et retourne les lignes de r�sultat dans un tableau associatif.
	 *
	 * Cette fonction permet d'ex�cuter une requ�te de type SELECT (ou EXEC de procstock qui retourne
	 * des lignes de donn�es), d'en m�moriser imm�diatement toutes les lignes de donn�es et de les retourner
	 * sous la forme d'un tableau (de tableaux associatifs). Le cache de requ�te peut �tre
	 * d�sactiv� si n�cessaire, m�me s'il est actif par d�faut. Dans ce cas, les r�sultats de la requ�te
	 * ne seront pas stock�s en m�moire et :
	 *  - Si la requ�te est de nouveau ex�cut�e, et qu'elle n'avait jamais �t� ex�cut�e avec le cache actif,
	 * elle sera envoy�e au serveur et mise en cache.
	 *  - Si la requ�te est de nouveau ex�cut�e et qu'elle avait �t� mise en cache auparavant, elle ne sera plus
	 * ex�cut�e et les r�sultats cach�s pr�c�demment seront sortis.
	 *
	 * @param $sql contient le texte de la requ�te.
	 * @param $forceNoCache permet de ne pas utiliser le cache de requ�te (lorsque TRUE), m�me s'il est actif.
	 * @return le tableau contenant toutes les lignes de r�sultats.
	 */
	function select ($sql, $forceNoCache = FALSE)
	{
		if ($this->cached && !$forceNoCache) {
			// On cache les requ�tes SQL. L'identifiant d'une requ�te est le md5 de sa version sans espaces.
			$id = md5 (preg_replace('/\s/', '', $sql));
			if (array_key_exists($id, $this->resultCache)) {
				return $this->resultCache[$id];
			}
		}

		// TODO: Journaliser la requ�te et les infos sur le r�sultat.

		$result = $this->uexec ($sql);
		$res = $result->fetchAll();

		if ($this->cached && !$forceNoCache) {
			$this->resultCache[$id] = $res;
		}

		return $res;
	}

	/**
	 * Ex�cute une requ�te SQL.
	 * Toute requ�te ex�cut�e via cette m�thode ne sera pas mise en cache.
	 *
	 * @param $sql contient le texte de la requ�te SQL � ex�cuter.
	 * @return un objet DbResult qui peut �tre utilis� pour r�cup�rer les
	 * lignes de r�sultats �ventuelles.
	 * @throw DatabaseException lorsque la commande n'a pu �tre envoy�e au serveur.
	 */
	function exec ($sql)
	{
		if ( ($res = @ sybase_query ($sql, $this->handle)) === FALSE) {
			if ($this->tranCount > 0) {
				$this->rollbackTran();
			}
			throw new DatabaseException ("exec() failed.", $sql);
		}
		$this->messageHandler (0, 0, 0, 0, 'exec: '. $sql . ' - ' . sybase_affected_rows());

		return new DbResult ($this, $res);
	}

	/**
	 * Ex�cute une requ�te SQL en mode unbuffered.
	 * Toute requ�te ex�cut�e via cette m�thode ne sera pas mise en cache.
	 *
	 * @param $sql contient le texte de la requ�te SQL � ex�cuter.
	 * @return un objet DbResult qui peut �tre utilis� pour r�cup�rer les
	 * lignes de r�sultats �ventuelles.
	 * @throw DatabaseException lorsque la commande n'a pu �tre envoy�e au serveur.
	 */
	function uexec ($sql)
	{

		if ( ($res = @ sybase_unbuffered_query ($sql, $this->handle)) === FALSE) {
			if ($this->tranCount > 0) {
				$this->rollbackTran();
			}
			throw new DatabaseException ("uexec() failed.", $sql);
		}
		$this->messageHandler (0, 0, 0, 0, 'uexec: '. $sql . ' - ' . sybase_affected_rows());

		return new DbResult ($this, $res);
	}

	function beginTran() {
		$this->exec ("begin transaction");
		$this->tranCount++;
	}

	function commitTran() {
		$this->exec ("commit transaction");
		$this->tranCount--;
	}

	function rollbackTran() {
		$this->exec ("rollback transaction");
		$this->tranCount--;
	}
};
?>
